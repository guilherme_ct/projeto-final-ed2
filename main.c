#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include<sys/time.h>
#include <locale.h>
#include "arvores.h"

double getTime();

int main()
{
    setlocale(LC_ALL,"Portuguese");

    char *texto;
    char *tok;
    Linha ln;

    FILE* file = fopen("massaDados.csv", "r");
    int i=0;
    char line[1024];

    int x; // será usado como retorno cod. erro
    arvAVL *raiz;
    arvoreLLRB *raizLLRB;

    raiz = cria_arvAVL();
    raizLLRB = cria_arvoreLLRB();

    int valores_a_buscar[] = {41884, 75036, 92675, 13783, 4935, 98999, 21040, 97830, 42107, 58389};
    //int filhos_a_remover[10];

    double tmp1,tmp2,tmp3,tmp4; // será usado para armazenar o tempo de execução

    // ************************ ARVORE AVL

    printf("\n\n -------------------- ARVORE AVL --------------------\n\n");

    tmp1=getTime();

    while (fgets(line, 1024, file) != NULL)
    {
        if(i>0){
            texto = strdup(line);

            // codigo
            tok = strtok(texto, ";"); //Isola o primeiro token
            ln.cod = atoi(strdup(tok));

            // nome
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.nome=strdup(tok);

            // idade
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.idade = atoi(strdup(tok));

            // empresa
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.empresa=strdup(tok);

            // departamento
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.departamento=strdup(tok);

            // salario
            tok = strtok(NULL, "\n"); //Isola o proximo token
            ln.salario = atof(strdup(tok));

            // limpar
            free(texto);

            insere_arvAVL(raiz, ln);
        }
        i++;
    }

    if(vazia_arvAVL(raiz)){
        printf(" A árvore AVL está vazia.");
    }else{
        printf(" A árvore AVL  possui elementos.");
    }
    printf("\n");

    x = totalNO_arvAVL(raiz);
    printf(" Total de nós na árvore AVL: %d", x);

    printf("\n Busca na Árvore AVL:\n\n");

    for(i=0;i<10;i++){
        consulta_pai_e_filhos(raiz, valores_a_buscar[i]);
    }

    printf("\n ---- Processo de remoção...\n\n");

    removeFilhoEsquerdoAVL(raiz, 41884);
    removeFilhoDireitoAVL(raiz, 75036);
    //removeFilhoEsquerdoAVL(raiz, 92675);
    //removeFilhoDireitoAVL(raiz, 13783);
    removeFilhoEsquerdoAVL(raiz, 4935);
    //removeFilhoEsquerdoAVL(raiz, 98999);
    //removeFilhoDireitoAVL(raiz, 21040);
    //removeFilhoDireitoAVL(raiz, 97830);
    //removeFilhoDireitoAVL(raiz, 42107);
    removeFilhoEsquerdoAVL(raiz, 58389);

    printf("\n ---- Consulta após remoção...\n\n");

    for(i=0;i<10;i++){
        consulta_pai_e_filhos(raiz, valores_a_buscar[i]);
    }

    liberar_arvAVL(raiz);

    tmp2=getTime();

    printf("\n\n");



    // ************************************* ARVORE RUBRO-NEGRA

    printf(" -------------------- ARVORE RUBRO-NEGRA --------------------\n\n");

    fclose(file);
    file = fopen("massaDados.csv", "r");

    tmp3=getTime();
    i=0;
    while (fgets(line, 1024, file) != NULL)
    {
        if(i>0){
            texto = strdup(line);
            // codigo
            tok = strtok(texto, ";"); //Isola o primeiro token
            ln.cod = atoi(strdup(tok));

            // nome
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.nome=strdup(tok);

            // idade
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.idade = atoi(strdup(tok));

            // empresa
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.empresa=strdup(tok);

            // departamento
            tok = strtok(NULL, ";"); //Isola o proximo token
            ln.departamento=strdup(tok);

            // salario
            tok = strtok(NULL, "\n"); //Isola o proximo token
            ln.salario = atof(strdup(tok));

            // limpar
            free(texto);

            insere_arvoreLLRB(raizLLRB, ln);
        }
        i++;
    }

    if(vazia_arvoreLLRB(raizLLRB)){
        printf(" A árvore Rubro-Negra está vazia.");
    }else{
        printf(" A árvore Rubro-Negra possui elementos.");
    }

    x = totalNO_arvoreLLRB(raizLLRB);
    printf("\n Total de nós na árvore Rubro-Negra: %d", x);

    printf("\n\n Busca na Árvore Rubro-Negra:\n\n");

    for(i=0;i<10;i++){
       consulta_pai_e_filhos_LLRB(raizLLRB, valores_a_buscar[i]);
    }


    printf("\n ---- Processo de remoção...\n\n");

    removeFilhoEsquerdoLLRB(raizLLRB, 41884);
    removeFilhoDireitoLLRB(raizLLRB, 75036);
    removeFilhoEsquerdoLLRB(raizLLRB, 92675);
    removeFilhoDireitoLLRB(raizLLRB, 13783);
    removeFilhoEsquerdoLLRB(raizLLRB, 4935);
    removeFilhoEsquerdoLLRB(raizLLRB, 98999);
    removeFilhoDireitoLLRB(raizLLRB, 21040);
    removeFilhoDireitoLLRB(raizLLRB, 97830);
    removeFilhoDireitoLLRB(raizLLRB, 42107);
    removeFilhoEsquerdoLLRB(raizLLRB, 58389);


    printf("\n ---- Consulta após remoção...\n\n");

    for(i=0;i<10;i++){
       consulta_pai_e_filhos_LLRB(raizLLRB, valores_a_buscar[i]);
    }

    liberar_arvoreLLRB(raizLLRB);

    tmp4 = getTime();

    printf("----------------------------------------------------------");

    printf("\n\n Tempo de execução da Arvore AVL: %f",tmp2-tmp1);
    printf("\n\n Tempo de execução da Arvore Rubro-Negra: %f\n\n",tmp4-tmp3);


    return 0;
}

//Função que sera utilizada para calcular o tempo de execução em segundos
double getTime(){
    struct timeval tv;
    gettimeofday(&tv, 0);
    return tv.tv_sec + tv.tv_usec/1e6;
}

