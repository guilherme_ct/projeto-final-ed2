#include <stdio.h>
#include <stdlib.h>
#include "arvores.h"

#define RED 1
#define BLACK 0

struct NO{
    Linha info;
    struct NO *esq;
    struct NO *dir;
    int cor;
};

arvoreLLRB *cria_arvoreLLRB(){
    arvoreLLRB *raiz = (arvoreLLRB*) malloc(sizeof(arvoreLLRB));
    if(raiz != NULL){
        *raiz = NULL;
    }
    return raiz;
}

void liberar_arvoreLLRB(arvoreLLRB *raiz){
    if(raiz == NULL){
        return;
    }
    libera_NO(*raiz);
    free(raiz);
}


int vazia_arvoreLLRB(arvoreLLRB *raiz){
    if(raiz == NULL){
        return 1;
    }
    if(*raiz == NULL){
        return 1;
    }
    return 0;
}

int altura_arvoreLLRB(arvoreLLRB *raiz){
    if(raiz == NULL){
        return 0;
    }
    if(*raiz == NULL){
        return 0;
    }
    int alt_esq = altura_arvoreLLRB(&((*raiz)->esq));
    int alt_dir = altura_arvoreLLRB(&((*raiz)->dir));
    if(alt_esq > alt_dir){
        return (alt_esq + 1);
    }else{
        return(alt_dir + 1);
    }
}

int totalNO_arvoreLLRB(arvoreLLRB *raiz){
    if(raiz == NULL){
        return 0;
    }
    if(*raiz == NULL){
        return 0;
    }
    int alt_esq = totalNO_arvoreLLRB(&((*raiz)->esq));
    int alt_dir = totalNO_arvoreLLRB(&((*raiz)->dir));
    return(alt_esq + alt_dir + 1);
}


struct NO *rotacionaEsquerda(struct NO *A){
    struct NO *B = A->dir;
    A->dir = B->esq;
    B->esq = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
}

struct NO *rotacionaDireita(struct NO *A){
    struct NO *B = A->esq;
    A->esq = B->dir;
    B->dir = A;
    B->cor = A->cor;
    A->cor = RED;
    return B;
}

struct NO *move2EsqRED(struct NO *H){
    trocaCor(H);
    if(cor(H->dir->esq) == RED){
        H->dir = rotacionaDireita(H->dir);
        H = rotacionaEsquerda(H);
        trocaCor(H);
    }
    return H;
}

struct NO *move2DirRED(struct NO *H){
    trocaCor(H);
    if(cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
        trocaCor(H);
    }
    return H;
}

struct NO *procuraMenor(struct NO *atual){
    struct NO *no1 = atual;
    struct NO *no2 = atual->esq;
    while(no2 != NULL){
        no1 = no2;
        no2 = no2->esq;
    }
    return no1;
}

struct NO *balancear(struct NO *H){
    if(cor(H->dir) == RED){
        H = rotacionaEsquerda(H);
    }
    if(H->esq != NULL && cor(H->dir) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
}

struct NO *removeMenor(struct NO *H){
    if(H->esq == NULL){
        free(H);
        return NULL;
    }
    if(cor(H->esq) == BLACK && cor(H->esq->esq) == BLACK){
        H = move2EsqRED(H);
    }
    H->esq = removeMenor(H->esq);
    return balancear(H);
}


struct NO *removeNO(struct NO *H, int valor){
    if(valor < H->info.cod){
        if(cor(H->esq) == BLACK && cor(H->esq->esq) == BLACK){
            H = move2EsqRED(H);
        }
        H->esq = removeNO(H->esq, valor);
    }else{
        if(cor(H->esq) == RED){
            H = rotacionaDireita(H);
        }
        if(valor == H->info.cod && (H->dir == NULL)){
            free(H);
            return NULL;
        }
        if(cor(H->dir) == BLACK && cor(H->dir->esq) == BLACK){
            H = move2DirRED(H);
        }
        if(valor == H->info.cod){
            struct NO *x = procuraMenor(H->dir);
            H->info = x->info;
            H->dir = removeMenor(H->dir);
        }else{
            H->dir = removeNO(H->dir, valor);
        }
    }
    return balancear(H);
}

int consulta_arvoreLLRB(arvoreLLRB *raiz, int valor){
    if(raiz == NULL){
        return 0;
    }
    struct NO *atual = *raiz;
    while(atual != NULL){
        if(valor == atual->info.cod){
            return 1;
        }
        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
    return 0;
}

int consulta_pai_e_filhos_LLRB(arvoreLLRB *raiz, int valor){
    if(raiz == NULL){
        printf("Raiz nula.");
        return 0;
    }
    struct NO *atual = *raiz;
    while(atual != NULL){
        if(valor == atual->info.cod){
            printf(" Pai: %d - %s\n",atual->info.cod,atual->info.nome);
            if(atual->dir){
                printf(" Filho direito: %d - %s\n",atual->dir->info.cod, atual->dir->info.nome);
            } else {
                printf(" Filho direito: Sem filhos à direita.\n");
            }
            if(atual->esq){
                printf(" Filho esquerdo: %d - %s\n\n",atual->esq->info.cod, atual->esq->info.nome);
            } else {
                printf(" Filho esquerdo: Sem filhos à esquerda.\n");
            }
            printf("\n");

        }
        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }

    }
    return 0;
}

int cor(struct NO *H){
    if(H == NULL){
        return BLACK;
    }else{
        return H->cor;
    }
}

void trocaCor(struct NO *H){
    H->cor =! H->cor;
    if(H->esq != NULL){
        H->esq->cor =! H->esq->cor;
    }
    if(H->dir != NULL){
        H->dir->cor =! H->dir->cor;
    }
}



int remove_arvoreLLRB(arvoreLLRB *raiz, int valor){
    if(consulta_arvoreLLRB(raiz, valor)){
        struct NO *H = *raiz;
        // função responsável pela busca pelo nó a ser removido
        *raiz = removeNO(H, valor);
        if(*raiz != NULL){
            (*raiz)->cor = BLACK;
        }
        return 1;
    }else{
        return 0;
    }
}




struct NO *insereNO(struct NO *H, Linha valor, int *resp){
    if(H == NULL){
        struct NO *novo;
        novo = (struct NO*) malloc(sizeof(struct NO));
        if(novo == NULL){
            *resp = 0;
            return NULL;
        }
        novo->info.cod = valor.cod;
        novo->info.departamento = valor.departamento;
        novo->info.empresa = valor.empresa;
        novo->info.idade = valor.idade;
        novo->info.nome = valor.nome;
        novo->info.salario = valor.salario;
        novo->cor = RED;
        novo->dir = NULL;
        novo->esq = NULL;
        *resp = 1;
        return novo;
    }
    if(valor.cod == H->info.cod){
        *resp = 0;
    }else{
        if(valor.cod < H->info.cod){
            H->esq = insereNO(H->esq, valor, resp);
        }else{
            H->dir = insereNO(H->dir, valor, resp);
        }
    }
    if(cor(H->dir) == RED && cor(H->esq) == BLACK){
        H = rotacionaEsquerda(H);
    }
    if(cor(H->esq) == RED && cor(H->esq->esq) == RED){
        H = rotacionaDireita(H);
    }
    if(cor(H->esq) == RED && cor(H->dir) == RED){
        trocaCor(H);
    }
    return H;
}

int insere_arvoreLLRB(arvoreLLRB *raiz, Linha valor){
    int resp;
    // função responsável pela busca do local de inserção do nó
    *raiz = insereNO(*raiz, valor, &resp);
    if((*raiz) != NULL){
        (*raiz) ->cor = BLACK;
    }
    return resp;
}


int removeFilhoDireitoLLRB(arvoreLLRB *raiz, int valor){
    int x;
    if(raiz == NULL){
        printf("Raiz nula");
    }
    struct NO *atual = *raiz;
    while(atual != NULL){
        if(valor == atual->info.cod){

            if(atual->dir!=NULL){
                x=remove_arvoreLLRB(raiz, atual->dir->info.cod);
                if(x){
                    printf("%d - %s: Filho à direita removido com sucesso\n\n",atual->info.cod,atual->info.nome);
                }
            } else {
                printf("%d - %s: Sem filho à direita para remover\n\n",atual->info.cod,atual->info.nome);
            }

        }
        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
}


int removeFilhoEsquerdoLLRB(arvoreLLRB *raiz, int valor){
    int x;
    if(raiz == NULL){
        printf("Raiz nula");
    }
    struct NO *atual = *raiz;
    while(atual != NULL){
        if(valor == atual->info.cod){

            if(atual->esq!=NULL){
                x=remove_arvoreLLRB(raiz, atual->esq->info.cod);
                if(x){
                    printf("%d - %s: Filho à esquerda removido com sucesso\n\n",atual->info.cod, atual->info.nome);
                }
            } else {
                printf("%d - %s: Sem filho à esquerda para remover\n\n",atual->info.cod, atual->info.nome);
            }

        }
        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
}

