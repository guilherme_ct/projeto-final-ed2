#include <stdio.h>
#include <stdlib.h>
#include "arvores.h"

struct NO{
    Linha info;
    int alt; // inclui altura da sub-arvore
    struct NO *esq;
    struct NO *dir;
};

arvAVL *cria_arvAVL(){
    arvAVL *raiz = (arvAVL*) malloc(sizeof(arvAVL));
    if(raiz != NULL){
        *raiz = NULL;
    }
    return raiz;
}

void liberar_arvAVL(arvAVL *raiz){
    if(raiz == NULL){
        return;
    }
    libera_NO(*raiz);
    free(raiz);
}

void libera_NO(struct NO *no){
    if(no == NULL){
        return;
    }
    libera_NO(no->esq);
    libera_NO(no->dir);
    free(no);
    no = NULL;
}

int vazia_arvAVL(arvAVL *raiz){
    if(raiz == NULL){
        return 1;
    }
    if(*raiz == NULL){
        return 1;
    }
    return 0;
}

int altura_arvAVL(arvAVL *raiz){
    if(raiz == NULL){
        return 0;
    }
    if(*raiz == NULL){
        return 0;
    }
    int alt_esq = altura_arvAVL(&((*raiz)->esq));
    int alt_dir = altura_arvAVL(&((*raiz)->dir));
    if(alt_esq > alt_dir){
        return (alt_esq + 1);
    }else{
        return(alt_dir + 1);
    }
}

// Calcular a altura de um nó
int alt_no(struct NO *no){
    if(no == NULL){
        return -1;
    }else{
        return no->alt;
    }
}

// Caluclar o fator de Balanceamento de um nó
int fatorBalanceamento_NO(struct NO *no){
    return labs(alt_no(no->esq) - alt_no(no->dir));
}
// a função labs(), arredonda valores em módulo

// calcular maior valor
int maior(int x, int y){
    if(x > y){
        return(x);
    }else{
        return(y);
    }
}

int totalNO_arvAVL(arvAVL *raiz){
    if(raiz == NULL){
        return 0;
    }
    if(*raiz == NULL){
        return 0;
    }
    int alt_esq = totalNO_arvAVL(&((*raiz)->esq));
    int alt_dir = totalNO_arvAVL(&((*raiz)->dir));
    return(alt_esq + alt_dir + 1);
}


// rotação simples à direita
void rotacaoLL(arvAVL *raiz){
    struct NO *no;
    no = (*raiz)->esq;
    (*raiz)->esq = no->dir;
    no->dir = *raiz;
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior(alt_no(no->esq), (*raiz)->alt + 1);
    *raiz = no;
}

void rotacaoRR(arvAVL *raiz){
    struct NO *no;
    no = (*raiz)->dir;
    (*raiz)->dir = no->esq;
    no->esq = (*raiz);
    (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    no->alt = maior(alt_no(no->dir), (*raiz)->alt) + 1;
    (*raiz) = no;
}

void rotacaoLR(arvAVL *raiz){
    rotacaoRR(&(*raiz)->esq);
    rotacaoLL(raiz);
}

void rotacaoRL(arvAVL *raiz){
    rotacaoLL(&(*raiz)->dir);
    rotacaoRR(raiz);
}

int consulta_arvAVL(arvAVL *raiz, int valor){
    if(raiz == NULL){
        return 0;
    }
    struct NO *atual = *raiz;
    while(atual != NULL){
        if(valor == atual->info.cod){
            return valor;
        }
        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
    return 0;
}

void consulta_pai_e_filhos(arvAVL *raiz, int valor){
    if(raiz == NULL){
        printf("Raiz nula.");
        return 0;
    }
    struct NO *atual = *raiz;
    while(atual != NULL){
        if(valor == atual->info.cod){
            printf(" Pai: %d - %s\n", atual->info.cod,atual->info.nome);

            if(atual->dir){
                printf(" Filho direito: %d - %s\n", atual->dir->info.cod,atual->dir->info.nome);
            } else {
                printf(" Filho direito: Sem filho a direita\n");
            }

            if(atual->esq){
                printf(" Filho esquerdo: %d - %s\n", atual->esq->info.cod,atual->esq->info.nome);
            } else {
                printf(" Filho esquerdo: Sem filho a esquerda\n");
            }
            printf("\n");

        }
        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }
    }
    return 0;
}

int insere_arvAVL(arvAVL *raiz, Linha valor){
    int res; // Pega resposta das chamadas de função
    if(*raiz == NULL){  // árvore vazia ou nó folha
        struct NO *novo;
        novo = (struct NO*) malloc(sizeof(struct NO));
        if(novo == NULL){
            return 0;
        }
        novo->info.cod = valor.cod;
        novo->info.nome = valor.nome;
        novo->info.idade = valor.idade;
        novo->info.empresa = valor.empresa;
        novo->info.departamento = valor.departamento;
        novo->info.salario = valor.salario;
        novo->alt = 0;
        novo->esq = NULL;
        novo->dir = NULL;
        *raiz = novo;
        return 1;
    }
    struct NO *atual = *raiz;
    if(valor.cod < atual->info.cod){
        if((res = insere_arvAVL(&(atual->esq), valor)) == 1){
            if(fatorBalanceamento_NO(atual) >= 2){
                if(valor.cod < (*raiz)->esq->info.cod){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
    }else{
        if(valor.cod > atual->info.cod){
            if((res = insere_arvAVL(&(atual->dir), valor)) == 1){
                if(fatorBalanceamento_NO(atual) >= 2){
                    if((*raiz)->dir->info.cod < valor.cod){
                        rotacaoRR(raiz);
                    }else{
                        rotacaoRL(raiz);
                    }
                }
            }
        }else{
            printf("Valor duplicado!\n");
            return 0;
        }
    }
    atual->alt = maior(alt_no(atual->esq), alt_no(atual->dir)) + 1;
    return res;
}

//função auxiliar - procura nó mais a esquerda
struct NO *procuramenor(struct NO *atual){
    struct NO *no1 = atual;
    struct NO *no2 = atual->esq;
    while(no2 != NULL){
        no1 = no2;
        no2 = no2->esq;
    }
    return no1;
}

// Função responsável pela busca do Nó a ser removido
int remove_arvAVL(arvAVL* raiz, Linha valor){
    if(*raiz == NULL){
        return 0;
    }
    int res;
    if(valor.cod < (*raiz)->info.cod){
        if((res = remove_arvAVL(&(*raiz)->esq, valor)) == 1){
            if(fatorBalanceamento_NO(*raiz) >= 2){
                if(alt_no((*raiz)->dir->esq <= alt_no((*raiz)->dir->dir))){
                    rotacaoRR(raiz);
                }else{
                    rotacaoRL(raiz);
                }
            }
        }
    }
    if((*raiz)->info.cod < valor.cod){
        if((res = remove_arvAVL(&(*raiz)->dir, valor)) == 1){
            if(fatorBalanceamento_NO(*raiz) >= 2){
                if(alt_no((*raiz)->esq->dir)<= alt_no((*raiz)->esq->esq)){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
    }
    if((*raiz)->info.cod == valor.cod){
        if(((*raiz)->esq == NULL) || (*raiz)->dir == NULL){
            struct NO *no_velho = (*raiz);

            if((*raiz)->esq != NULL){
                *raiz = (*raiz)->esq;
            }else{
                *raiz = (*raiz)->dir;
            }
            free(no_velho);
        }else{
            struct NO *temp = procuramenor((*raiz)->dir);
            (*raiz)->info = temp->info;
            remove_arvAVL((*raiz)->dir, (*raiz)->info);
            if(fatorBalanceamento_NO(*raiz) >= 2){
                if(alt_no((*raiz)->esq->dir) <= alt_no((*raiz)->esq->esq)){
                    rotacaoLL(raiz);
                }else{
                    rotacaoLR(raiz);
                }
            }
        }
        if(*raiz != NULL){
            (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
        }
        return 1;
    }
    if(*raiz != NULL){
        (*raiz)->alt = maior(alt_no((*raiz)->esq), alt_no((*raiz)->dir)) + 1;
    }

    return res;
}


int removeFilhoDireitoAVL(arvAVL *raiz, int valor){
    int x;
    if(raiz == NULL){
        return 0;
    }
    struct NO *atual = *raiz;
    while(atual != NULL){

        if(valor == atual->info.cod){

            if(atual->dir != NULL){
                x = remove_arvAVL(raiz, atual->dir->info);
                if(x){
                    printf("%d - %s: Filho à direita removido com sucesso\n\n",atual->info.cod,atual->info.nome);
                }
            }else {
                printf("%d - %s: Sem filho à direita para remover\n\n",atual->info.cod,atual->info.nome);
            }

        }

        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }

    }
    return 0;
}

int removeFilhoEsquerdoAVL(arvAVL *raiz, int valor){
    int x;
    if(raiz == NULL){
        return 0;
    }
    struct NO *atual = *raiz;
    while(atual != NULL){
        if(valor == atual->info.cod){

            if(atual->esq != NULL){
                x = remove_arvAVL(raiz, atual->esq->info);
                if(x){
                    printf("%d - %s: Filho à esquerda removido com sucesso\n\n",atual->info.cod,atual->info.nome);
                }
            }else {
                printf("%d - %s: Sem filho à esquerda para remover\n\n",atual->info.cod,atual->info.nome);
            }
        }
        if(valor > atual->info.cod){
            atual = atual->dir;
        }else{
            atual = atual->esq;
        }

    }
    return 0;

}


