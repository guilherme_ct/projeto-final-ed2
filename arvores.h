typedef struct linha
{
   int cod;
   char *nome;
   int idade;
   char *empresa;
   char *departamento;
   float salario;
} Linha;

//AVL
typedef struct NO *arvAVL;

arvAVL *cria_arvAVL();

int insere_arvAVL(arvAVL *raiz, Linha valor);

int altura_arvAVL(arvAVL *raiz);

int vazia_arvAVL(arvAVL *raiz);

struct NO *procuramenor(struct NO *atual);

void consulta_pai_e_filhos(arvAVL *raiz, int valor);

int remove_arvAVL(arvAVL* raiz, Linha valor);

int fatorBalanceamento_NO(struct NO *no);

int alt_no(struct NO *no);

void rotacaoRL(arvAVL *raiz);

void rotacaoLR(arvAVL *raiz);

void rotacaoRR(arvAVL *raiz);

void rotacaoLL(arvAVL *raiz);

struct NO *procuramenor(struct NO *atual);

int maior(int x, int y);

int removeFilhoDireitoAVL(arvAVL *raiz, int valor);

int removeFilhoEsquerdoAVL(arvAVL *raiz, int valor);


// RUBRO NEGRA
typedef struct NO *arvoreLLRB;

arvoreLLRB *cria_arvoreLLRB();

void liberar_arvoreLLRB(arvoreLLRB *raiz);

int vazia_arvoreLLRB(arvoreLLRB *raiz);

int altura_arvoreLLRB(arvoreLLRB *raiz);

int totalNO_arvoreLLRB(arvoreLLRB *raiz);

int insere_arvoreLLRB(arvoreLLRB *raiz, Linha valor);

int remove_arvoreLLRB(arvoreLLRB *raiz, int valor);

int consulta_arvoreLLRB(arvoreLLRB *raiz, int valor);

int removeFilhoDireitoLLRB(arvoreLLRB *raiz, int valor);

int removeFilhoEsquerdoLLRB(arvoreLLRB *raiz, int valor);

